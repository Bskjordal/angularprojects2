import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SayingService } from './service/saying.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [SayingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
