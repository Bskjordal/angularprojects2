import { Component } from '@angular/core';
import { Saying } from './models/saying.model';
import { SayingService } from './service/saying.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'saying-app';
  categories: Array<string> = [];
  sayings: Array<string> = [];
  match: Array<Saying> = [];

  constructor(private sayingsService: SayingService) { }
  ngOnInit() {
    this.categories = this.sayingsService.getCategories();
  }

  onSelectCat(event: any): void {
    const selectedCategory = event.target.value;

    if (selectedCategory == "") {
      this.match = [];

    } else {
      this.match = this.sayingsService.getSayingsThatMatchCategory(selectedCategory);
    }

  }

  onPersonClicked(person:string): void{    
    this.match = this.sayingsService.getSayingsByPerson(person);

  }

}
