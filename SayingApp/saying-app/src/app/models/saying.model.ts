export class Saying {
   
    public category: string = '';
    public quote: string = '';
    public name: string = '';

    constructor(category: string, quote: string, name: string) {        
        this.category = category;
        this.quote = quote;
        this.name = name;
    }
}

