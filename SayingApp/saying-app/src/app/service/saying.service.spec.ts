import { TestBed } from '@angular/core/testing';

import { SayingService } from './saying.service';

describe('SayingService', () => {
  let service: SayingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SayingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
