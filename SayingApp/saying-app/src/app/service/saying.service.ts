import { Injectable } from '@angular/core';
import { Saying } from '../models/saying.model'



@Injectable({
  providedIn: 'root'
})
export class SayingService {
  categories: Array<string>;
  sayings: Array<Saying>;

  constructor() {    
    this.sayings = [
      new Saying("Staying Safe", "An apple a day keeps the doctor away.","Brian Skjordal"),
      new Saying("Staying Safe", "A ship in a harbor is safe, but that is not what ships are for.","John Smith")
    ];
    this.categories = ["Staying Safe"];
  }
  public getCategories(): Array<string> {
    return this.categories;
  }

  public getSayings(): Array<Saying> {
    return this.sayings;
  }
  public getSayingsThatMatchCategory(category: string)
    : Array<Saying> {
    let matching =
      this.sayings.filter(s => s.category == category);
    return matching;
  }
  public getSayingsByPerson(name: string)
  
    : Array<Saying> {
      let nameMatch =
        this.sayings.filter(s => s.name == name);
      return nameMatch;
  }
}

