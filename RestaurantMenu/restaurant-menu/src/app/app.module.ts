import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuComponent } from './menu/menu.component';

const appRoutes: Routes = [
  { path: "", component: MenuComponent },
  { path: "menu-item", component: MenuItemComponent },
  
];

@NgModule({
  declarations: [
    AppComponent,
    MenuItemComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes), 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
