import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent {
  menuId!:any;
  name!: any;
  price!: any;
  desc!:any;
  
  menuItems = [
    { id: 1, name: 'Burger', description: 'A classic hamburger',
    price: 5.99 },
    { id: 2, name: 'Fries', description: 'Crispy french fries',
    price: 2.99 },
    { id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
    }
    ];

    constructor(
      private activatedRoute: ActivatedRoute,
      
      ) {}
      
      ngOnInit() {
      this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.menuId = this.getName(id);
      this.name = (this.menuId.name);
      this.desc = (this.menuId.description);
      this.price = (this.menuId.price);
        
      });
      }
      
      getName(id: number) {
      return this.menuItems.find((menuId) => menuId.id == id);
      }
}
