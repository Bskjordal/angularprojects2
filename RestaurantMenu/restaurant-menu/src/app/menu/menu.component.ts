import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  constructor(private Router: Router) { }
  menuItems = [
    { id: 1, name: 'Burger', description: 'A classic hamburger',
    price: 5.99 },
    { id: 2, name: 'Fries', description: 'Crispy french fries',
    price: 2.99 },
    { id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
    }
    ];

    Listitem(selectedValue: string): void {
      this.Router.navigate(['/menu-item'],
        {
          queryParams: {
            id: selectedValue
          }
        }
      );
}
}

  
