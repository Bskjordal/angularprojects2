import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'first-app';
  today: string = "";
  msg:string = "";

  items = [
    { id: 1, name: 'Item 1', price: 10.99 },
    { id: 2, name: 'Item 2', price: 19.99 },
    { id: 3, name: 'Item 3', price: 4.99 },
    { id: 4, name: 'Item 4', price: 8.99 }
    ];
    filterText = '';
    sortField = '';
    sortOrder = '';



  ngOnInit() {
    let rightNow = new Date();
    this.today = rightNow.toLocaleDateString();
    let hours = rightNow.getHours();
    
       
    if (hours >= 5 && hours <= 11) {
      this.msg = "It's a wonderful morning";      
    } else if (hours >=12 && hours <=17) {
       this.msg = "It's a great afternoon";
    } else if (hours >=18 && hours <=23) {
      this.msg = "It's a nice evening";
    } else if (hours >=0 && hours <= 4) {
      this.msg = "It's deep night -- why are you awake?";
    }
  }
}

