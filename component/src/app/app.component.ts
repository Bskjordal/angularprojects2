import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TESTING INPUT METHOD';
  PARA = 'Testing a paragraph';
  name = ' ';

  sayHello(name: string) {
    console.log(`Hello, ${name}!`);
    this.name = name;
  }
}
