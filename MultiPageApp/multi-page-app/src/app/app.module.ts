import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { ContactUsComponent }
  from './contact-us/contact-us.component';
import { ProductsComponent }
  from './products/products.component';
import { UserInputComponent } from './user-input/user-input.component';
import { WordCounterComponent } from './word-counter/word-counter.component';
import { MathCalculatorComponent } from './math-calculator/math-calculator.component';
import { OutdoorActivitiesComponent } from './outdoor-activities/outdoor-activities.component';
import { BooksComponent } from './books/books.component';
import { FormExerciseComponent } from './form-exercise/form-exercise.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "products", component: ProductsComponent },
  { path: "contact-us", component: ContactUsComponent },
  { path: "user-input", component: UserInputComponent },
  { path: "word-counter", component: WordCounterComponent },
  { path: "math-calculator", component: MathCalculatorComponent },
  { path: "outdoor-activities", component: OutdoorActivitiesComponent },
  { path: "books", component: BooksComponent },
  { path: "form-exercise", component: FormExerciseComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactUsComponent,
    ProductsComponent,
    UserInputComponent,
    WordCounterComponent,
    MathCalculatorComponent,
    OutdoorActivitiesComponent,
    BooksComponent,
    FormExerciseComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
