import { Component } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
  selector: 'app-form-exercise',
  templateUrl: './form-exercise.component.html',
  styleUrls: ['./form-exercise.component.css']
})
export class FormExerciseComponent {
  Contacts: Array<Contact> = [];
  newContacts: Contact = new Contact("","","");

  constructor() {
    this.Contacts = [
      new Contact("Brian Skjordal", "b@cox.net", "123-456-9999"),
      new Contact("John Smith", "j@cox.net", "111-222-3333")

    ];
  }
  onSubmit() {
    this.Contacts.push(this.newContacts);
    console.log(`Saving contact:`, JSON.stringify(this.Contacts));
    this.newContacts = new Contact("","","");
  }
}

