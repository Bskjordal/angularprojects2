import { Component } from '@angular/core';

@Component({
  selector: 'app-word-counter',
  templateUrl: './word-counter.component.html',
  styleUrls: ['./word-counter.component.css']
})
export class WordCounterComponent {

  title = 'Word Counter';
  textEntered: any;
  residesIn: string = "";
  wordCount: any;
  words: any;

  countWords() {
    this.wordCount = this.textEntered ? this.textEntered.split(/\s+/) : 0;
    this.words = this.wordCount ? this.wordCount.length : 0;
  }

}
