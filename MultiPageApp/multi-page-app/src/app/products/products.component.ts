import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
 
  prod1ImgUrl: string = 'assets/honda.jfif';
  prod1AltTxt:string = 'honda trx450r';
  prod2ImgUrl: string = 'assets/yamaha.jfif';
  prod2AltTxt:string = 'yahaha 450';
  prod3ImgUrl: string = 'assets/yamaha2.jfif';
  prod3AltTxt:string = 'yahaha 450';


}
