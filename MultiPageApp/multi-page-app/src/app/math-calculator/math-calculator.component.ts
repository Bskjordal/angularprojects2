import { Component } from '@angular/core';

@Component({
  selector: 'app-math-calculator',
  templateUrl: './math-calculator.component.html',
  styleUrls: ['./math-calculator.component.css']
})
export class MathCalculatorComponent {

  operand1: number = 0;
  operand2: number = 0;
  answer: number = 0;
  answerIsAvailable: boolean = false;


  onAddClicked(): void {
    this.answer = this.operand1 + this.operand2;
    this.answerIsAvailable = true;
  }
  onSubtractClicked(): void {
    this.answer = this.operand1 - this.operand2;
    this.answerIsAvailable = true;
  }
  onMultiplyClicked(): void {
    this.answer = this.operand1 * this.operand2;
    this.answerIsAvailable = true;
  }
  onDivideClicked(): void {
    this.answer = this.operand1 / this.operand2;
    this.answerIsAvailable = true;
  }
  onClearClicked(): void {
    this.operand1 = 0;
    this.operand2 = 0;
    this.answer = 0;
    this.answerIsAvailable = false;
  }

}


