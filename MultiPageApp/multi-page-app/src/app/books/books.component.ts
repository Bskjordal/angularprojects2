import { Component } from '@angular/core';
import { Book } from '../models/book.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent {
  Books : Array<Book> = [];

  constructor(){
    this.Books = [
      new Book("Fairy Tale","Stephen King","1234567"),
      new Book("Something","Stephen King","33333367"),
      new Book("Made Up Name","John King","36663333367")

    ];
  }
}
