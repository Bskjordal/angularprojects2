import { Component, OnInit } from '@angular/core';
import { Todo } from '../models/todo.model';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-to-dos',
  templateUrl: './to-dos.component.html',
  styleUrls: ['./to-dos.component.css']
})
export class ToDosComponent implements OnInit{
  todoArray: Array<Todo> = [];

  constructor(private TodoService: TodoService) { }

  ngOnInit(): void {
    this.TodoService.getTodo().subscribe(data => {
      this.todoArray = data;
    })
  }
}
