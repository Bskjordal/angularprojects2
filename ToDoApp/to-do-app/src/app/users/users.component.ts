import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserServiceService } from '../service/user-service.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  UserArray: Array<User> = [];

   constructor(private UserServiceService: UserServiceService) { }

  ngOnInit(): void {
    this.UserServiceService.getUser().subscribe(data => {      
      this.UserArray = data;      
    })
  }
}
