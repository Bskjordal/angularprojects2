import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  getTodo(): Observable<Todo[]> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/todos/', this.httpOptions)
      .pipe(map((res) => <Todo[]>res));
  }
}
