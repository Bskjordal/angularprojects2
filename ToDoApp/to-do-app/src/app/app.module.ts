import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ToDosComponent } from './to-dos/to-dos.component';
import { TodoService } from './service/todo.service';
import { UserServiceService } from './service/user-service.service';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './users/users.component';

const appRoutes: Routes = [
  { path: "to-dos", component: ToDosComponent },
  { path: "header", component: HeaderComponent },
  { path: "app.component", component: AppComponent },
  { path: "users", component: UsersComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ToDosComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,HttpClientModule
  ],
  providers: [TodoService, UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
