import { Component } from '@angular/core';
import { RandomnumberService } from './service/randomnumber.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'random-service';
  ranNumber: number = 0;
  min:number = 1;
  max:number = 100;

  constructor(private randomNumber: RandomnumberService) {   
    this.ranNumber = this.randomNumber.getRandomNumber(this.min,this.max);
    
  }
}
