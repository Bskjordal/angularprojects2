import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomnumberService {
  
  constructor() { }

  public getRandomNumber(min:number,max:number) {
    return Math.floor(Math.random() * (max - min) + min);

  }
}
