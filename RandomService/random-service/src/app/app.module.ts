import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RandomnumberService } from './service/randomnumber.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [RandomnumberService],
  bootstrap: [AppComponent]
})
export class AppModule { }
