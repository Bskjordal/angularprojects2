import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'input-output-exercises';
  msg   = ""

  books = ['Book 1', 'Book 2', 'Book 3'];
  clickEdit(title: string) {
    console.log(`Editing book: ${title}`);
    this.msg=`Editing book: ${title}`
     
  }
}
