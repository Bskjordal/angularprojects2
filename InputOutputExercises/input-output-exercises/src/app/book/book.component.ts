import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent {
  @Input() title: string = "";
  @Output() onClickEdit = new EventEmitter<string>();
  onEdit() {
    this.onClickEdit.emit(this.title);
  }
}
